function posnegz()
{   
    var t1 : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var n : number = parseInt(t1.value);
    var pos : number = 0;
    var neg : number = 0;
    var zero: number = 0;
    var s1 : number = 0;
    var s2 : number = 0;
    var s3 : number = 0;
    
    var i : number;
    for(i=0;i<n;i++){
        var x:number = parseInt(prompt("Enter a number "));
        console.log(x);
        if(x>0){
            pos=pos+1;
            s1=s1+x;    
        }
        else if(x<0){
            neg=neg+1;
            s2=s2+x;
        }
        else if(x==0){
            zero=zero+1;
            s3=s3+x;
        }
    }

    document.getElementById("show").innerHTML="<b>Answer:</b>";
    document.getElementById("show").innerHTML+="<br><b>Number of positive elements:</b>"+pos;
    document.getElementById("show").innerHTML+="<br><b>Sum of positive elements:</b>"+s1;
    document.getElementById("show").innerHTML+="<br><b>Number of negative elements:</b>"+neg;
    document.getElementById("show").innerHTML+="<br><b>Sum of negative elements:</b>"+s2;
    document.getElementById("show").innerHTML+="<br><b>Number of zeroes:</b>"+zero;
    
}

