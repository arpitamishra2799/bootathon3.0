function coordinate() {
    //Initializing the vextices
    var t11 = document.getElementById("t11");
    var t12 = document.getElementById("t12");
    var t21 = document.getElementById("t21");
    var t22 = document.getElementById("t22");
    var t31 = document.getElementById("t31");
    var t32 = document.getElementById("t32");
    var t41 = document.getElementById("t41");
    var t42 = document.getElementById("t42");
    //Converting vertices into number format
    var x1 = parseFloat(t11.value);
    var y1 = parseFloat(t12.value);
    var x2 = parseFloat(t21.value);
    var y2 = parseFloat(t22.value);
    var x3 = parseFloat(t31.value);
    var y3 = parseFloat(t32.value);
    var x = parseFloat(t41.value);
    var y = parseFloat(t42.value);
    //Finding area of each triangle
    var a = Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2;
    var a1 = Math.abs(x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2)) / 2;
    var a2 = Math.abs(x1 * (y - y3) + x * (y3 - y1) + x3 * (y1 - y)) / 2;
    var a3 = Math.abs(x1 * (y2 - y) + x2 * (y - y1) + x * (y1 - y2)) / 2;
    //Finding sum and equating..
    var sum = a1 + a2 + a3;
    var ans;
    console.log(a, a1, a2, a3, sum);
    if (Math.abs(a - sum) < 0.000001) {
        ans = "Point is Inside the Triangle";
    }
    else {
        ans = "Point is Outside the Triangle";
    }
    document.getElementById("here").innerHTML = "<u><b>" + ans + "</b></u>";
}
//# sourceMappingURL=coordinate.js.map