//Initialisation of values
var x1:HTMLInputElement=<HTMLInputElement>document.getElementById("x1"); //input of x coordinate of first point from the user in the form of string
var y1:HTMLInputElement=<HTMLInputElement>document.getElementById("y1"); //input of y coordinate of first point from the user in the form of string
var x2:HTMLInputElement=<HTMLInputElement>document.getElementById("x2"); //input of x coordinate of second point from the user in the form of string
var y2:HTMLInputElement=<HTMLInputElement>document.getElementById("y2"); //input of y coordinate of second point from the user in the form of string
var x3:HTMLInputElement=<HTMLInputElement>document.getElementById("x3"); //input of x coordinate of third point from the user in the form of string
var y3:HTMLInputElement=<HTMLInputElement>document.getElementById("y3"); //input of y coordinate of third point  from the user in the form of string
var ar1:HTMLInputElement=<HTMLInputElement>document.getElementById("calc"); 
function area() // Calculating the area of triangle using Heron's Formula
 {
     //Coversion of the string values into float values
     var x01:number=parseFloat(x1.value);
     var y01:number=parseFloat(y1.value);
     var x02:number=parseFloat(x2.value);
     var y02:number=parseFloat(y2.value);
     var x03:number=parseFloat(x3.value);
     var y03:number=parseFloat(y3.value);

     //Finding the length of sides s1, s2 and s3
     var s1:number=Math.sqrt(Math.pow(x02-x01,y02-y01));
     var s2:number=Math.sqrt(Math.pow(x01-x03,y01-y03));
     var s3:number=Math.sqrt(Math.pow(x03-x02,y03-y02));

    //Calculating semi-perimeter s
    var s:number=(s1+s2+s3)/2

     //Calculating the area of a triangle
     var area:number=Math.sqrt(s*(s-s1)*(s-s2)*(s-s3));

     //Displaying the area
     ar1.value=area.toString();
 
 }