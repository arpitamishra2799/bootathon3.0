var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
var t4 = document.getElementById("t4");
var res = document.getElementById("res");
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString();
}
function sub() {
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    t3.value = c.toString();
}
function mul() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}
function div() {
    try {
        var c = parseFloat(t1.value) / parseFloat(t2.value);
        t3.value = c.toString();
    }
    catch (error) {
        t3.value = (error);
    }
}
function sin() {
    var c = Math.sin(parseFloat(t4.value));
    res.value = c.toString();
}
function cos() {
    var c = Math.cos(parseFloat(t4.value));
    res.value = c.toString();
}
function tan() {
    var c = Math.tan(parseFloat(t4.value));
    res.value = c.toString();
}
function pow() {
    var c = Math.pow(parseFloat(t1.value), parseFloat(t2.value));
    t3.value = c.toString();
}
function sqrt() {
    var c = Math.sqrt(parseFloat(t1.value));
    t3.value = c.toString();
}
//# sourceMappingURL=calc.js.map