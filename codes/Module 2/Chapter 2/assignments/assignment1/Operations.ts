var s1 : HTMLInputElement = <HTMLInputElement>document.getElementById("s1");
var s2 : HTMLInputElement = <HTMLInputElement>document.getElementById("s2");
var s3 : HTMLInputElement = <HTMLInputElement>document.getElementById("s3");
var s4 : HTMLInputElement = <HTMLInputElement>document.getElementById("s4");


function operation()
{
    var s : string = s1.value;
    var start : number = parseInt(s2.value);
    var end : number = parseInt(s3.value);
    var ch : string = s4.value;
    //finding substring
    var sub : string = s.substring(start,end);
    //searching character
    var pos: number = s.search(ch);
    //printing answers
    document.getElementById("Result").innerHTML = "<br><b>RESULT:<b><br>";
    document.getElementById("Result").innerHTML += "<br><b>Original String : <b>" + s+"<br>";
    document.getElementById("Result").innerHTML += "<br><b>Substring : <b>" +sub+"<br>";
    document.getElementById("Result").innerHTML += "<br><b>Position of character is : <b>" + pos+"<br>";
    
}