var s1 = document.getElementById("s1");
var s2 = document.getElementById("s2");
var s3 = document.getElementById("s3");
var s4 = document.getElementById("s4");
function operation() {
    var s = s1.value;
    var start = parseInt(s2.value);
    var end = parseInt(s3.value);
    var ch = s4.value;
    //finding substring
    var sub = s.substring(start, end);
    //searching character
    var pos = s.search(ch);
    //printing answers
    document.getElementById("Result").innerHTML = "<br><b>RESULT:<b><br>";
    document.getElementById("Result").innerHTML += "<br><b>Original String : <b>" + s + "<br>";
    document.getElementById("Result").innerHTML += "<br><b>Substring : <b>" + sub + "<br>";
    document.getElementById("Result").innerHTML += "<br><b>Position of character is : <b>" + pos + "<br>";
}
//# sourceMappingURL=Operations.js.map