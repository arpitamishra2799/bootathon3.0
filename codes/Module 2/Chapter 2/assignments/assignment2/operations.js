function operation() {
    var s = document.getElementById("s1");
    var a = s.value;
    var t;
    document.getElementById("show").innerHTML = "<br><b>The Original String is the : </b>" + a;
    //Converting the string to Upper Case
    t = a.toUpperCase();
    document.getElementById("show").innerHTML += "<br><b> Upper Case  : </b> " + t;
    //Converting the string to Lower Case
    t = a.toLowerCase();
    document.getElementById("show").innerHTML += "<br><b>Lower Case : </b> " + t;
    //Splitting the string
    let ar = a.split(" ");
    document.getElementById("show").innerHTML += "<br><b>Splitted  : </b> " + ar;
}
//# sourceMappingURL=operations.js.map