function operation()
{
    var s:HTMLInputElement=<HTMLInputElement>document.getElementById("s1");
    var a: string=s.value;
    var t: string;
    document.getElementById("show").innerHTML="<br><b>The Original String is the : </b>" + a ; 

    //Converting the string to Upper Case
    t=a.toUpperCase();
    document.getElementById("show").innerHTML+="<br><b> Upper Case  : </b> " +t;
    
    //Converting the string to Lower Case
    t=a.toLowerCase();
    document.getElementById("show").innerHTML+="<br><b>Lower Case : </b> " +t;

    //Splitting the string
    let ar=a.split(" ");
    document.getElementById("show").innerHTML+="<br><b>Splitted  : </b> " +ar;
}